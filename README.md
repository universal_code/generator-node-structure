# generator-node-structure [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url] [![Coverage percentage][coveralls-image]][coveralls-url]
> Estructura de un proyecto en Node.js

## Installation

First, install [Yeoman](http://yeoman.io) and generator-node-structure using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-node-structure
```

Then generate your new project:

```bash
yo node-structure
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

ISC © [Roberto Tinajero]()


[npm-image]: https://badge.fury.io/js/generator-node-structure.svg
[npm-url]: https://npmjs.org/package/generator-node-structure
[travis-image]: https://travis-ci.org/UNIVERSALcode/generator-node-structure.svg?branch=master
[travis-url]: https://travis-ci.org/UNIVERSALcode/generator-node-structure
[daviddm-image]: https://david-dm.org/UNIVERSALcode/generator-node-structure.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/UNIVERSALcode/generator-node-structure
[coveralls-image]: https://coveralls.io/repos/UNIVERSALcode/generator-node-structure/badge.svg
[coveralls-url]: https://coveralls.io/r/UNIVERSALcode/generator-node-structure
