var express = require('express');
var router  = express.Router();
var homeController  = require('../controllers/home.controller');
var aboutController = require('../controllers/about.controller');

// export router
module.exports = router;

router.get('/', homeController.showHome);

router.get('/', aboutController.showAbout);
