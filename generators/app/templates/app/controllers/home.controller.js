module.exports = {

  // show the home page
  showHome: (req, res) => {
    res.render('pages/index', {
      title: 'Home'
    });
  }

};
