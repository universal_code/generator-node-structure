// Example model
<% if(database == 'mongodb'){ %>
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ArticleSchema = new Schema({
  title: String,
  url: String,
  text: String
});

ArticleSchema.virtual('date')
  .get(() => this._id.getTimestamp());

mongoose.model('Article', ArticleSchema);<% } %>
<% if(database == 'mysql' || database == 'postgresql' || database == 'sqlite'){ %>
module.exports = (sequelize, DataTypes) => {

  var Article = sequelize.define('Article', {
    title: DataTypes.STRING,
    url: DataTypes.STRING,
    text: DataTypes.STRING
  }, {
    classMethods: {
      associate: (models) => {
        // example on how to add relations
        // Article.hasMany(models.Comments);
      }
    }
  });

  return Article;
};
<% } %><% if(database == 'none'){ %>
function Article (opts) {
  if(!opts) opts = {};
  this.title = opts.title || '';
  this.url = opts.url || '';
  this.text = opts.text || '';
}

module.exports = Article;
<% } %><% if(database == 'rethinkdb'){%>
var thinky = require('../../config/thinky');
var r = thinky.r;
var type = thinky.type;

var Article = thinky.createModel('Article', {
  title: String,
  url: String,
  text: String
});

module.exports = Article;

// example on how to add relations
// var Comment = require('./comment');
// Article.hasMany(Comment, 'comments', 'id', 'article_id');
<% } %>
