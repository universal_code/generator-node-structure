'use strict'
// == SET UP ===================================================================
var express       = require('express'),          // HTTP server
    session       = require('express-session'),  // Store the session data on the server
    logger        = require('morgan'),           // HTTP request logger
    cookieParser  = require('cookie-parser'),    // Simple cookie-based session middleware
    bodyParser    = require('body-parser'),
    methodOverride = require('method-override'), // Lets you use HTTP verbs such as PUT or DELETE in places where the client doesn't support it.
    compress      = require('compression'),      // Compress a HTTP message
    path          = require('path'),             // Provides utilities for working with file and directory paths.
    flash         = require('connect-flash'),    // The flash is a special area of the session used for storing messages
    favicon       = require('serve-favicon'),
    dotenv        = require('dotenv').config(),
    glob          = require('glob');
    app           = express(),                  // Create a web application
    port          = process.env.PORT || 3000, 	// Set the port
    view          = process.env.VIEW || 'ejs',
    env           = process.env.NODE_ENV || 'development',
    routes        = require('./app/routes/routes.js');
var sessionStore = new session.MemoryStore;

module.exports = function(app, config) {
  var env = process.env.NODE_ENV || 'development';
  app.locals.ENV = env;
  app.locals.ENV_DEVELOPMENT = env == 'development';

  app.set('views', config.root + '/app/views/pages');
  app.set('view engine', 'ejs');


  <% if(viewEngine=='ejs'){%>
    app.set('views', path.join(__dirname, 'app/views/ejs'));  // Set path of view engine
    app.set('view engine', '<%= viewEngine %>');  // Set up ejs for templating
  <% }%><% if (viewEngine == 'pug'){ %>
    app.set('views', path.join(__dirname, 'app/views/pug'));  // Set path of view engine
    app.set('view engine', '<%= viewEngine %>');  // Set up ejs for templating
  <% }%><% if (viewEngine == 'pug'){ %>
    app.set('views', path.join(__dirname, 'app/views/handlebars'));  // Set path of view engine
    app.set('view engine', '<%= viewEngine %>');  // Set up ejs for templating
  <% } %>

  // app.use(favicon(config.root + '/public/img/favicon.ico'));
  app.use(logger('dev'));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(cookieParser());
  app.use(compress());
  app.use(express.static(config.root + '/public'));
  app.use(methodOverride());

  var controllers = glob.sync(config.root + '/app/controllers/*.js');
  controllers.forEach(function (controller) {
    require(controller)(app);
  });

  app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  if(app.get('env') === 'development'){
    app.use(function (err, req, res, next) {
      res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: err,
        title: 'error'
      });
    });
  }

  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: {},
        title: 'error'
      });
  });

  return app;
};
