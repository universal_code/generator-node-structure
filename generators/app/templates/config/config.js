var path = require('path');
var rootPath = path.normalize(__dirname + '/..');
var env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: '<%= slugify(appname) %>'
    },
    port: process.env.PORT || 3000,<% if(database == 'mongodb'){ %>
    db: 'mongodb://localhost/<%= slugify(appname) %>-development'<% } %><% if(database == 'mysql'){ %>
    db: 'mysql://localhost/<%= slugify(appname) %>-development'<% } %><% if(database == 'postgresql'){ %>
    db: 'postgres://localhost/<%= slugify(appname) %>-development'<% } %><% if(database == 'sqlite'){ %>
    db: 'sqlite://localhost/<%= slugify(appname) %>-development',
    storage: rootPath + '/data/<%= slugify(appname) %>-development'<% } %>
  },

  test: {
    root: rootPath,
    app: {
      name: '<%= slugify(appname) %>'
    },
    port: process.env.PORT || 3000,<% if(database == 'mongodb'){ %>
    db: 'mongodb://localhost/<%= slugify(appname) %>-test'<% } %><% if(database == 'mysql'){ %>
    db: 'mysql://localhost/<%= slugify(appname) %>-test'<% } %><% if(database == 'postgresql'){ %>
    db: 'postgres://localhost/<%= slugify(appname) %>-test'<% } %><% if(database == 'sqlite'){ %>
    db: 'sqlite://localhost/<%= slugify(appname) %>-test',
    storage: rootPath + '/data/<%= slugify(appname) %>-test'<% } %>
  },

  production: {
    root: rootPath,
    app: {
      name: '<%= slugify(appname) %>'
    },
    port: process.env.PORT || 3000,<% if(database == 'mongodb'){ %>
    db: 'mongodb://localhost/<%= slugify(appname) %>-production'<% } %><% if(database == 'mysql'){ %>
    db: 'mysql://localhost/<%= slugify(appname) %>-production'<% } %><% if(database == 'sqlite'){ %>
    db: 'sqlite://localhost/<%= slugify(appname) %>-production',
    storage: rootPath + 'data/<%= slugify(appname) %>-production'<% } %><% if(database == 'postgresql'){ %>
    db: 'postgres://localhost/<%= slugify(appname) %>-production'<% } %>
  }
};

module.exports = config[env];
