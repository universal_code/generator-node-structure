var gulp = require('gulp');
var $    = require('gulp-load-plugins')();
var imagemin    = require('gulp-imagemin');
var browserSync = require('browser-sync').create();
var cleanCSS    = require('gulp-clean-css');
var os          = require('os');
var pkg         = require('./package.json');
var reload = browserSync.reload;
let dev = true;
// Set the banner content

var browsers = os.platform() === 'linux' ? 'google-chrome' : (
  os.platform() === 'darwin' ? 'Google chrome' : (
  os.platform() === 'win32' ? 'chrome' : 'firefox'));

gulp.task('styles', function() {<% if (includeCSS == 'sass') { %>
  gulp.src('./app/assets/sass/main.sass')
      .pipe($.plumber())
      .pipe($.if(dev, $.sourcemaps.init()))
      .pipe($.sass())<% } if (includeCSS == 'less') { %>
  gulp.src('./app/assets/less/main.less')
      .pipe($.plumber())
      .pipe($.if(dev, $.sourcemaps.init()))
      .pipe($.less())<% } if (includeCSS == 'stylus') { %>
  gulp.src('./app/assets/less/main.less')
      .pipe($.plumber())
      .pipe($.if(dev, $.sourcemaps.init()))
      .pipe($.stylus())<% } %>
      .pipe($.autoprefixer({browsers: ['last 1 version']}))
      .pipe(gulp.dest('public/css'))
      .pipe(browserSync.reload({stream: true}));
});

// Minify compiled CSS
gulp.task('minify-css', ['styles'], function() {
  gulp.src(['public/css/main.css'])
      .pipe($.sourcemaps.init())
      .pipe(cleanCSS({compatibility: 'ie8'}))
      .pipe($.rename({suffix: '.min'}))
      .pipe($.sourcemaps.write())
      .pipe(gulp.dest('./public/css'))
      .pipe(browserSync.reload({ stream: true }))
});

gulp.task('minify-img', () =>{
  gulp.src('assets/img/*')
      .pipe(imagemin([
        imagemin.jpegtran({progressive: true}),
        imagemin.optipng({optimizationLevel: 5})
      ]))
      .pipe(gulp.dest('public/img'))
      .pipe(browserSync.reload({ stream: true }))
});

gulp.task('minify-js', function(){
  gulp.src('./app/assets/js/**/*.js')
      .pipe($.plumber())
      .pipe($.uglify())
      .pipe($.rename({suffix: '.min'}))
      .pipe(gulp.dest('public/js'))
      .pipe(browserSync.reload({stream: true}));
});

// Copy lib files from /node_modules into /lib
// NOTE: requires `npm install` before running!
gulp.task('copy', function() {
  gulp.src([
      'node_modules/bootstrap/dist/**/*',
      '!**/npm.js',
      '!**/bootstrap-theme.*'
    ])
    .pipe(gulp.dest('public/lib/bootstrap'))

  gulp.src(['node_modules/jquery/dist/jquery.js', 'node_modules/jquery/dist/jquery.min.js'])
    .pipe(gulp.dest('public/lib/jquery'))

  gulp.src([
      'node_modules/font-awesome/**',
      '!node_modules/font-awesome/**/*.map',
      '!node_modules/font-awesome/.npmignore',
      '!node_modules/font-awesome/*.txt',
      '!node_modules/font-awesome/*.md',
      '!node_modules/font-awesome/*.json'
    ])
    .pipe(gulp.dest('public/lib/font-awesome'))
});

gulp.task('nodemon', function (cb) {
	var started = false;
	return $.nodemon({
		script: 'bin/www',
	}).on('start', function () {
		if (!started) {
			cb();
			started = true;
		}
	}).on('restart', function onRestart() {
		// reload connected browsers after a slight delay
		setTimeout(function reload() {
			browserSync.reload({
				stream: true
			});
		}, 500);
	});
});

// Configure the browserSync task
gulp.task('browserSync', ['nodemon'], function() {
  browserSync.init(null,{
    proxy: "localhost:3000/",
    port: 7000,
    browser: browsers,
    open: true
  });
})

gulp.task('default', ['styles','minify-css','minify-js','minify-img','copy']);

// Dev task with browserSync
gulp.task('dev', ['browserSync', 'default'], function() {<% if(includeCSS == 'sass'){ %>
  gulp.watch('assets/sass/*.sass', ['sass']);<% } %><% if(includeCSS == 'less'){ %>
  gulp.watch('assets/sass/*.less', ['less']);<% } %><% if(includeCSS == 'stylus'){ %>
  gulp.watch('assets/sass/*.styl', ['stylus']);<% } %>
  gulp.watch('public/css/*.css', ['minify-css']);
  gulp.watch('assets/js/*.js', ['minify-js']);<% if(includeEngine == 'ejs'){ %>
  gulp.watch('app/views/**/*.ejs', browserSync.reload);<% } %><% if(includeEngine == 'pug'){ %>
  gulp.watch('app/views/**/*.pug', browserSync.reload);<% } %><% if(includeEngine == 'handlebars'){ %>
  gulp.watch('app/views/**/*.hbs', browserSync.reload);<% } %>
  gulp.watch('js/**/*.js', browserSync.reload);
});
