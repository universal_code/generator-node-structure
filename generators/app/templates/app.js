'use strict'
// == SET UP ===================================================================
var express       = require('express'),          // HTTP server
    session       = require('express-session'),  // Store the session data on the server
    logger        = require('morgan'),           // HTTP request logger
    cookieParser  = require('cookie-parser'),    // Simple cookie-based session middleware
    bodyParser    = require('body-parser'),
    methodOverride = require('method-override'), // Lets you use HTTP verbs such as PUT or DELETE in places where the client doesn't support it.
    path          = require('path'),             // Provides utilities for working with file and directory paths.
    flash         = require('connect-flash'),    // The flash is a special area of the session used for storing messages
    glob          = require('glob'),
    favicon       = require('serve-favicon'),
    dotenv        = require('dotenv').config(),<% if(database == 'mongodb'){ %>
    mongoose      = require('mongoose'),<% } %><% if(database == 'mysql' || database == 'postgresql' || database == 'sqlite'){ %>
    db            = require('./app/models'),<% } %>
    app           = express(),                  // Create a web application
    port          = process.env.PORT || 3000, 	// Set the port
    view          = process.env.VIEW || 'ejs',
    env           = process.env.NODE_ENV || 'development',
    routes        = require('./app/routes/routes.js');
var sessionStore = new session.MemoryStore;
var pathview  = 'app/views'

<% if(viewEngine=='ejs'){%>
app.set('views', path.join(__dirname, pathview));  // Set path of view engine
app.set('view engine', '<%= viewEngine %>');  // Set up ejs for templating
<% }%><% if (viewEngine == 'pug'){ %>
app.set('views', path.join(__dirname, pathview));  // Set path of view engine
app.set('view engine', '<%= viewEngine %>');  // Set up ejs for templating
<% }%><% if (viewEngine == 'handlebars'){ %>
app.set('views', path.join(__dirname, pathview));  // Set path of view engine
app.set('view engine', 'hbs');  // Set up ejs for templating
<% } %>

app.use(favicon(path.join(__dirname, 'public', 'img/favicon.png')));

app.use(logger('dev')); // Log every request to the console
app.use(methodOverride());
app.use(bodyParser.json()); // Get information from html forms
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser('supersecretkey'));  // Read cookies (needed for auth)
var expiryDate = new Date( Date.now() + 60 * 60 * 1000 ); // 1 hour
app.use(session({
  secret: 'supersecretkey',
  store: sessionStore,
  resave: false,
  saveUninitialized: true,
  cookie: {
    secure: true,
    httpOnly: true,
    expires: expiryDate,
    maxAge: 24 * 60 * 60 * 1000 // 24 hours
    //maxAge: 60000
  }
}));
app.use(express.static(path.join(__dirname, 'public')));  // set the static files location
app.use(flash()); // use connect-flash for flash messages stored in session
app.use(routes);

// == ROUTES ===================================================================



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
if(app.get('env') === 'development'){
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err,
      title: 'error'
    });
  });
}


app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('pages/error');
});

module.exports = app;
