'use strict';
var Yeoman  = require('yeoman-generator');
var chalk   = require('chalk');
var yosay   = require('yosay');
var util    = require('util');
var path    = require('path');
var mkdirp  = require('mkdirp');
var pkg     = require('../../package.json');
var glob    = require('glob');
var slugify = require('underscore.string/slugify');

module.exports = class extends Yeoman {
  constructor(args, opts) {
    super(args, opts);

    this.argument('appname', {
      type: String,
      required: false
    });
    this.slugify = slugify;
    this.env.options.appPath = this.options.appPath || 'app';
    this.config.set('appPath', this.env.options.appPath);
  }
  prompting() {
    var done = this.async();
    // Have Yeoman greet the user.
    this.log(yosay(
      'Welcome to the brilliant ' + chalk.red('generator-node-structure') + ' generator!'
    ));

    var prompt = [{
      type: 'confirm',
      name: 'createDirectory',
      message: chalk.gray('Create a new directory for your project?')
    }, {
      type: 'input',
      name: 'dirname',
      message: chalk.gray('What is your app\'s name?'),
      default: 'Project1'
    }, {
      type: 'checkbox',
      name: 'features',
      message: chalk.gray('Choose features for start your project:'),
      choices: [{
        name: 'Bootstrap',
        value: 'includeBootstrap',
        checked: true
      },{
        name: 'jQuery',
        value: 'includeJQuery',
        checked: true
      },{
        name: 'FontAwesome',
        value: 'includeFontAwesome',
        checked: true
      },{
        name: 'HighCharts',
        value: 'includeHighCharts',
        checked: true
      }],
      default: true
    }, {
      type: 'list',
      name: 'legacyBootstrap',
      message: chalk.gray('Choose version of Bootstrap:'),
      choices: [
        'Bootstrap 3',
        'Bootstrap 4'
      ],
      default: 'Bootstrap 4',
      store: true,
      when: answers => answers.features.indexOf('includeBootstrap') !== -1
    }, {
      type: 'list',
      name: 'viewEngine',
      message: chalk.gray('Select a view engine to use:'),
      choices: [
        'Pug',
        'Swing',
        'EJS',
        'Handlebars'
      ],
      default: 'ejs',
      store: true
    }, {
      type: 'list',
      name: 'cssPreprocessor',
      message: chalk.gray('Choose a CSS preprocessor to use:'),
      choices: [
        'None',
        'Sass',
        'less',
        'Stylus'
      ],
      store: true,
      default: 'Sass',
    }, {
      type: 'list',
      name: 'database',
      message: chalk.gray('Choose a database to use:'),
      choices: [
        'None',
        'MongoDB',
        'MySQL',
        'Oracle'
      ],
      store: true,
      default: 'MongoDB',
    }, {
      type: 'list',
      name: 'buildTool',
      message: chalk.gray('Choose a build tool to use:'),
      choices: [
        'Grunt',
        'Gulp',
      ],
      store: true,
      default: 'Gulp',
    }];

    return this.prompt(prompt).then((answers) => {
      this.createDirectory = answers.createDirectory;
      this.dirname = answers.dirname;
      var features = answers.features;
      var hasFeature = feat => features && features.indexOf(feat) !== -1;
      this.includeBootstrap = hasFeature('includeBootstrap');
      this.includeJQuery = hasFeature('includeJQuery');
      this.includeFontAwesome = hasFeature('includeFontAwesome');
      this.includeHighCharts = hasFeature('includeHighCharts');
      this.legacyBootstrap = answers.legacyBootstrap;
      this.viewEngine = answers.viewEngine.toLowerCase();
      this.cssPreprocessor = answers.cssPreprocessor.toLowerCase();
      this.database = answers.database.toLowerCase();
      this.buildTool = answers.buildTool.toLowerCase();
      done();
    });
  }

  writing() {
    this._createDirectory();
    this._writingPackageJSON();
    this._writingGit();
    this._writingBower();
    this._writingCSS();
    this._writingServer();
    if(this.buildTool=='gulp'){
      this._writingGulp();
    }else if (this.buildTool == 'grunt') {
      this._writingGrunt();
    }
  }
  _createDirectory(){
    if (this.createDirectory) {
       this.destinationRoot(this.dirname);
       this.appname = this.dirname;
     }else {
       this.appname = this.dirname;
     }
  };

  _writingPackageJSON(){
    this.fs.copyTpl(
      this.templatePath('_package.json'),
      this.destinationPath('package.json'),
      this
    );
  };

  _writingGit() {
    this.fs.copy(
      this.templatePath('gitignore'),
      this.destinationPath('.gitignore'));

    this.fs.copy(
      this.templatePath('gitattributes'),
      this.destinationPath('.gitattributes'));
  };

  _writingBower(){
    this.fs.copyTpl(
      this.templatePath('_bower.json'),
      this.destinationPath('bower.json'),
      this
    );
  };
  _writingGulp() {
    this.sourceRoot(path.join(__dirname, 'templates'));
    this.fs.copyTpl(this.templatePath('./gulpfile.js'),
      this.destinationPath('gulpfile.js'),{
        includeCSS: this.cssPreprocessor,
        includeEngine: this.viewEngine,
      }
    );
  };

  _writingCSS(){
    //Css
    let stylesheets = this.cssPreprocessor;
    if (stylesheets === 'none') stylesheets = 'css';
    if (stylesheets === 'sass') stylesheets = 'sass';
    if (stylesheets === 'less') stylesheets = 'less';
    if (stylesheets === 'stylus') stylesheets = 'stylus';

    this.sourceRoot(path.join(__dirname, 'templates', 'css', stylesheets));
    this.fs.copyTpl(this.templatePath('.'), this.destinationPath('assets',stylesheets), this);


  }

  _writingServer(){
    // bin/www
    this.sourceRoot(path.join(__dirname, 'templates','bin'));
    this.fs.copyTpl(this.templatePath('.'),this.destinationPath('bin'),this);
    // app.js
    this.sourceRoot(path.join(__dirname, 'templates'));
    this.fs.copyTpl(this.templatePath('app.js'),this.destinationPath('app.js'),this);
    // app/controllers
    this.sourceRoot(path.join(__dirname, 'templates', 'app/controllers'));
    this.fs.copyTpl(this.templatePath('.'), this.destinationPath('app/controllers'), this);
    // app/models
    this.sourceRoot(path.join(__dirname, 'templates', 'app/models'));
    this.fs.copyTpl(this.templatePath('.'), this.destinationPath('app/models'), this);
    // app/routes
    this.sourceRoot(path.join(__dirname, 'templates', 'app/routes'));
    this.fs.copyTpl(this.templatePath('.'), this.destinationPath('app/routes'), this);
    // app/views
    var views = this.viewEngine;
    this.sourceRoot(path.join(__dirname, 'templates', 'app/views', views));
    this.fs.copy(this.templatePath('.'), this.destinationPath('app/views'));
    // config
    this.sourceRoot(path.join(__dirname, 'templates', 'config'));
    this.fs.copyTpl(this.templatePath('.'), this.destinationPath('config'), this);

    this.sourceRoot(path.join(__dirname, 'templates', 'assets'));
    this.fs.copy(this.templatePath('.'),this.destinationPath('assets'));

    mkdirp.sync('public');
    mkdirp.sync('public/components');
    mkdirp.sync('public/js');
    mkdirp.sync('public/css');
    mkdirp.sync('public/img');

    // mkdirp.sync('assets/img');
    // mkdirp.sync('assets/img');
  }

  install() {
    this.installDependencies({
      npm: true,
      bower: true,
      yarn: false
    });
  }
  };
